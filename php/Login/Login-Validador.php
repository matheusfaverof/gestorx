<?php
/********************************************************************/
/**                                                                **/
/**                           GestorX                              **/
/**             Todos os Direitos Reselvados - 2023                **/
/**                                                                **/
/**       A Copia parcial ou total deste documento é proibida      **/
/**                                                                **/
/********************************************************************/
// Descrição: Valida a existencia da autenticação do Usuario
// @Autor: Matheus Favero


    // Valida se existe um Cookie com o nome 'user_account_id'
    // Caso contrario redireciona para a index.php
    
    if(!isset($_COOKIE['user_account_id'])){
        header('Location: ../../index.php');
        session_destroy();
        exit();
    }
?>