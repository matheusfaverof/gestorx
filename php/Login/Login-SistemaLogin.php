<?php
/********************************************************************/
/**                                                                **/
/**                           GestorX                              **/
/**             Todos os Direitos Reselvados - 2023                **/
/**                                                                **/
/**       A Copia parcial ou total deste documento é proibida      **/
/**                                                                **/
/********************************************************************/
// Descrição: Realiza a Autenticação do Usuario
// @Autor: Matheus Favero



session_start();
include('../../system/db/db_config.php');

// Guarda os dados do Post do usuario no index.php
$post_user_name = pg_escape_string($cconn, $_POST['user_post_name']);
$post_user_pass = pg_escape_string($cconn, $_POST['user_post_pass']);

// Query para buscar os dados de login no banco
$pg_query_login = "SELECT usua_codigo, usua_login FROM usua_usuario WHERE usua_login = '{$post_user_name}' AND usua_senha = '{$post_user_pass}'";

// Busca os dados no banco
$pg_result_login = pg_query($cconn, $pg_query_login);

// Verifica se o login está correto
// Se estiver correto, seta os Cookies no Navegador do Usuario
// @Param $pg_result_login Resultado da Query SQL
// @Return Login correto: Redireciona para a Home.php | Caso incorreto Recarrega a Pagina setando um Session

if(pg_num_rows($pg_result_login) >= 1){
    $result = pg_fetch_assoc($pg_result_login);
    setcookie("user_account_id", $result['usua_codigo'], time() + (21600 * 30), "/"); // 86400 = 1 day
    setcookie("user_account_login", $result['usua_login'], time() + (21600 * 30), "/"); // 86400 = 1 day
    header('Location: /Home.php');
} else {
    $_SESSION['login_error_login'] = 'invalid_user';
    header('Location: /index.php');
    exit();
}
?>