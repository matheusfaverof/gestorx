<?php 

function BuscaListaClientesGestor(){
    include('./system/db/db_config.php');                           //Importa o Arquivo de Configurações do Banco de dados
    $user_id = $_COOKIE['user_account_id'];                         //Busca o ID do Usuario nos Cookies

    #Verifica se existe um controle de Filtro
    #Se a Sessão Filtro-Cliente for setada com um código, significa que a pagina foi recarregada devido a seleção de filtro
    #Então seleciona o cliente anterior

    $Filtro_Cliente_Codigo = null;                                  //Cliente Selecionado no Filtro

    //Verifica se a Sessão existe
    if(isset($_SESSION['Filtro-Cliente'])){                     
        $Filtro_Cliente_Codigo = $_SESSION['Filtro-Cliente'][0];    //Define a Variavel com o valor da Sessão
        //unset($_SESSION['Filtro-Cliente']);                       //Deleta a sessão
    }


    $PG_QUERY_BUSCA_CLIENTES = pg_query($cconn,"SELECT cc.cli_codigo,cc.cli_nome FROM cg_cliente_gestor cg JOIN cli_clientes cc ON cc.cli_codigo = cg.cg_cli_codigo WHERE cg.cg_usua_codigo = $user_id");

    while($RESULT = pg_fetch_assoc($PG_QUERY_BUSCA_CLIENTES)){
        if($Filtro_Cliente_Codigo == $RESULT['cli_codigo']){
            echo '<option selected value="'.$RESULT["cli_codigo"].'">'.$RESULT["cli_nome"].'</option>';
        } else {
            echo '<option value="'.$RESULT["cli_codigo"].'">'.$RESULT["cli_nome"].'</option>';
        }
    }
}
?>