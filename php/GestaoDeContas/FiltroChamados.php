<?php 
session_start();

# Recebe o POST da pagina de chamados e valida os dados dos Filtros
# Após realizada um GET no WS e solicita atualizar os chamados do cliente necessário
# Seta as sessões com os valores
# Após retorna para a pagina de chamados

$Filtro_Texto = $_POST['filtro-busca'];                                  //Valor do Filtro de Texto
$Filtro_Cliente = $_POST['filtro-cliente'];                              //Valor do Seletor de Cliente

#Valida o Cliente Selecionado



if($Filtro_Cliente == 0 && $Filtro_Texto == ''){
    #Caso for código 0 (Todos os Clientes) e o Filtro Texto estiver Vazio (Null)
    //unset($_SESSION['Filtro-Cliente']);                                  //Destoi a sessão
    //header('Location: /GestaoDeContas.php');

    $RetornoWS = WS_AtualizaTodosOsClietes();

    if($RetornoWS == 'Cliente Atualizado'){
        unset($_SESSION['Filtro-Cliente']);                                  //Destoi a sessão
        sleep(2);
        header('Location: /GestaoDeContas.php');
    } else {
        echo 'Ocorreu um erro ao atualizar os dados do Cliente via WS!';
    }
} 
else if($Filtro_Cliente == 0 && $Filtro_Cliente != ''){
    #Caso for código 0 (Todos os Clientes) e o Filtro contenha um Texto
    //$_SESSION['Filtro-Cliente'] = [0,$Filtro_Texto];
    //header('Location: /GestaoDeContas.php');
    $RetornoWS = WS_AtualizaTodosOsClietes();

    if($RetornoWS == 'Cliente Atualizado'){
        $_SESSION['Filtro-Cliente'] = [0,$Filtro_Texto];
        sleep(2);
        header('Location: /GestaoDeContas.php');
    } else {
        echo 'Ocorreu um erro ao atualizar os dados do Cliente via WS!';
    }

} else {
    #Caso Haja um cliente selecionado no Filtro

    $RetornoWS = WS_AtualizaCliente($Filtro_Cliente);

    if($RetornoWS == 'Cliente Atualizado'){
        $_SESSION['Filtro-Cliente'] = [$Filtro_Cliente, $Filtro_Texto];      //Cria uma sessão com o codigo do cliente Selecionado
        sleep(2);
        header('Location: /GestaoDeContas.php');
    } else{
        echo 'Ocorreu um erro ao atualizar os dados do Cliente via WS!';
    }
}



function WS_AtualizaCliente($Codigo){
    $ch = curl_init('http://100.96.1.2:8080/BuscaChamadoPorCliente?Codigo='.$Codigo);

    curl_setopt_array($ch, [

        // Equivalente ao -X:
        CURLOPT_CUSTOMREQUEST => 'GET',
    
        // Permite obter o resultado
        CURLOPT_RETURNTRANSFER => 1,
    ]);    
    $resposta = json_decode(curl_exec($ch), true);
    curl_close($ch);
    return $resposta;
}

function WS_AtualizaTodosOsClietes(){
    $user_id = $_COOKIE['user_account_id'];                         //Busca o ID do Usuario nos Cookies
    include('../../system/db/db_config.php');

    $PG_QUERY_BUSCA_CLIENTES = pg_query($cconn,"SELECT * FROM cg_cliente_gestor cg JOIN cli_clientes cc ON cc.cli_codigo = cg.cg_cli_codigo WHERE cg.cg_usua_codigo = 1");

    
    while ($RESULT = pg_fetch_assoc($PG_QUERY_BUSCA_CLIENTES)){
        WS_AtualizaCliente($RESULT['cli_codigo']);
    }

    return 'Cliente Atualizado';
}
?>