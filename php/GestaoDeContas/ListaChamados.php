<?php 

function ListaChamadosClienteSemFiltro(){
    # Busca todos os Chamados dos Clientes associados ao Usuario Logado
    # Não considera filtros, sendo o primeiro carregamento da pagina

        include('./system/db/db_config.php');
        $user_id = $_COOKIE['user_account_id'];         #Busca o ID do Usuario nos Cookies

        $PG_QUERY_CHAMADOS = pg_query($cconn,"SELECT * FROM ccha_cliente_chamado ccha
        JOIN cli_clientes cc ON cc.cli_codigo = ccha.ccha_cli_codigo
        JOIN cg_cliente_gestor cg ON cg.cg_cli_codigo = cc.cli_codigo
        WHERE ccha.ccha_chamado_status != 'Concluido' OR ccha.ccha_chamado_status IS NULL
        AND ccha_otrs_fila in('Análise', 'Atendimento', 'Infraestrutura')
        AND cg.cg_usua_codigo = $user_id
        ORDER BY ccha.ccha_chamado_ticket desc");

        while($RESULT = pg_fetch_assoc($PG_QUERY_CHAMADOS)){
            //Variaveis do Resultado
            $cor_status = null;
            $cor_fonte  = null;
            $cor_linha  = null;
            $cor_linha_fonte = null;
            $cor_acao_icones = null;

            //Validações
            if($RESULT['ccha_chamado_status'] != 'Concluido' && $RESULT['ccha_otrs_estado'] == 'Encerrado'){
                $cor_linha = 'black';
                $cor_linha_fonte = 'white';
                $cor_acao_icones = 'white';
            }

            //Seletor de Cor do Status
            switch($RESULT["ccha_chamado_status"]){
                case "Novo":
                    $cor_status = "bg-novo";
                    $cor_fonte = "fonte-white";
                    break;
                case "Em Analise":
                    $cor_status = "bg-emanalise";
                    $cor_fonte = "fonte-white";
                    break;
                case "Pendente":
                    $cor_status = "bg-danger";
                    $cor_fonte = "fonte-white";
                    break;
                case "Aguardando ATT":
                    $cor_status = "bg-aguardandoatt";
                    $cor_fonte = "fonte-white";
                    break;
                case "Aguardando Cliente":
                    $cor_status = "bg-aguardandocliente";
                    $cor_fonte = "fonte-white";
                    break;
                case "Concluido":
                    $cor_status = "bg-concluido";
                    $cor_fonte = "fonte-black";
                    break;
            }


            //Constroi o Link do Chamado para direcionar ao OTRS
            $OTRS_Link = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?ChallengeToken=THGYujeIDdKWFvEa88aH1VmWDlLQ62nz&Action=AgentTicketSearch&Subaction=Search&EmptySearch=1&ShownAttributes=LabelFulltext%3BLabelTicketNumber&Profile=&Name=&Fulltext=&TicketNumber='.$RESULT["ccha_chamado_ticket"].'&Attribute=&ResultForm=Normal';

            echo '<tr style="background-color:'.$cor_linha.';color:'.$cor_linha_fonte.'">
                    <td scope="row">'.$RESULT["ccha_chamado_prioridade"].'</td>
                    <td>'.$RESULT["cli_nome"].'</td>
                    <td>'.$RESULT["ccha_otrs_data_cadastro"].'</td>
                    <td>'.$RESULT["ccha_otrs_data_alteracao"].'</td>
                    <td>'.substr($RESULT["ccha_otrs_proprietario"],0, strrpos($RESULT["ccha_otrs_proprietario"], '(')).'</td>
                    <td>'.$RESULT["ccha_chamado_titulo"].'</td>
                    <td>'.$RESULT["ccha_otrs_fila"].'</td>
                    <td>'.$RESULT["ccha_otrs_estado"].'</td>
                    <td class="'.$cor_status.' '.$cor_fonte.'">'.$RESULT["ccha_chamado_status"].'</td>
                    <td><a target="_blank" href="https://dev.azure.com/trafegus/Trafegus%20Transform/_workitems/edit/'.$RESULT["ccha_chamado_tarefa_cod"].'/">'.$RESULT["ccha_chamado_tarefa_cod"].'</a></td>
                    <td style="width: 110px;">
                    <div class="td-icon-bandeja">
                        <button class="td-icon-btn"><i class="bi bi-eye-fill td-icon" style="color:'.$cor_acao_icones.'"></i></button>
                        <a onclick="CriaModalEditarChamado('.$RESULT['ccha_codigo'].')" href="" class="td-icon-btn" data-bs-toggle="modal" data-bs-target="#modal-editar-chamado"><i class="bi bi-pencil-square td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                        <a class="td-icon-btn" href="'.$OTRS_Link.'" target="_blank" style="margin-left: 10px;"><i class="bi bi-box-arrow-right td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                    </div>
                    </td>
                </tr>';
        }

}

function ListaChamadosClienteComFiltro($Cliente_Codigo, $Filtro_Texto){
# Busca todos os Chamados do do cliente Solicitado
    # Considera o Filtro de Chamados
    # Considera o Filtro de Texto

    #Caso o Codigo do Cliente for 0 (Todos), deve-se buscar os chamados de todos os clientes associados 

    include('./system/db/db_config.php');
    $user_id = $_COOKIE['user_account_id'];         #Busca o ID do Usuario nos Cookies

            //Validação da Chamada do Método
            /*
            if($Cliente_Codigo == 0 || $Cliente_Codigo = null){
                $Cliente_Codigo = null;
            }
            if($Filtro_Texto == ''){
                $Filtro_Texto = null;
            }*/

    //Valida os dados da Chamado do Método
    #Se o Código do Cliente for Diferente de 0 existe um Cliente Selecionado
    #Então realiza a conexão e busca os chamados do cliente especifico
    if($Cliente_Codigo != 0){

        $PG_QUERY_CHAMADOS = "SELECT * FROM ccha_cliente_chamado ccha
            JOIN cli_clientes cc on cc.cli_codigo = ccha_cli_codigo
            JOIN cg_cliente_gestor cg ON cg.cg_cli_codigo = cc.cli_codigo
            WHERE (ccha_chamado_status != 'Concluido' or ccha_chamado_status is null)
            AND ccha_otrs_fila in('Análise', 'Atendimento', 'Infraestrutura')
            AND ccha_cli_codigo = $1
            AND (ccha_chamado_prioridade ilike $2
            OR ccha_otrs_data_cadastro = $3
            OR ccha_otrs_data_alteracao = $3
            OR ccha_chamado_ticket ilike $2
            OR ccha_chamado_titulo ilike $2
            OR ccha_otrs_fila ilike $2
            OR ccha_chamado_status ilike $2
            OR ccha_chamado_tarefa_cod ilike $2)
            ORDER BY ccha_chamado_ticket DESC";

        $PG_RESULT_CHAMADOS = pg_query_params($cconn, $PG_QUERY_CHAMADOS, array($Cliente_Codigo, '%'.$Filtro_Texto.'%',null));

    }

    #Se o Código do Cliente for igual a 0
    #Então realiza a conexão e busca os chamados de todos os clientes
    if($Cliente_Codigo == 0){
        $PG_QUERY_CHAMADOS = "SELECT * FROM ccha_cliente_chamado ccha
            JOIN cli_clientes cc on cc.cli_codigo = ccha_cli_codigo
            JOIN cg_cliente_gestor cg ON cg.cg_cli_codigo = cc.cli_codigo
            WHERE ccha_chamado_status != 'Concluido'
            AND ccha_otrs_fila in('Análise', 'Atendimento', 'Infraestrutura')
            AND ccha_cli_codigo in (SELECT ccha_cli_codigo FROM cg_cliente_gestor cg WHERE cg.cg_usua_codigo = $1)
            AND (ccha_chamado_prioridade ilike $2
            OR ccha_otrs_data_cadastro = $3
            OR ccha_otrs_data_alteracao = $3
            OR ccha_chamado_ticket ilike $2
            OR ccha_chamado_titulo ilike $2
            OR ccha_otrs_fila ilike $2
            OR ccha_chamado_status ilike $2
            OR ccha_chamado_tarefa_cod ilike $2)
            ORDER BY ccha_otrs_data_cadastro DESC";

        $PG_RESULT_CHAMADOS = pg_query_params($cconn, $PG_QUERY_CHAMADOS, array($user_id, '%'.$Filtro_Texto.'%',null));
    }



    while($RESULT = pg_fetch_assoc($PG_RESULT_CHAMADOS)){
        //Variaveis do Resultado
        $cor_status = null;
        $cor_fonte  = null;
        $cor_linha  = null;
        $cor_linha_fonte = null;
        $cor_acao_icones = null;

        //Validações
        if($RESULT['ccha_chamado_status'] != 'Concluido' && $RESULT['ccha_otrs_estado'] == 'Encerrado'){
            $cor_linha = 'black';
            $cor_linha_fonte = 'white';
            $cor_acao_icones = 'white';
        }

        //Seletor de Cor do Status
        switch($RESULT["ccha_chamado_status"]){
            case "Novo":
                $cor_status = "bg-novo";
                $cor_fonte = "fonte-white";
                break;
            case "Em Analise":
                $cor_status = "bg-emanalise";
                $cor_fonte = "fonte-white";
                break;
            case "Pendente":
                $cor_status = "bg-danger";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando ATT":
                $cor_status = "bg-aguardandoatt";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando Cliente":
                $cor_status = "bg-aguardandocliente";
                $cor_fonte = "fonte-white";
                break;
            case "Concluido":
                $cor_status = "bg-concluido";
                $cor_fonte = "fonte-black";
                break;
        }


        //Constroi o Link do Chamado para direcionar ao OTRS
        $OTRS_Link = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?ChallengeToken=THGYujeIDdKWFvEa88aH1VmWDlLQ62nz&Action=AgentTicketSearch&Subaction=Search&EmptySearch=1&ShownAttributes=LabelFulltext%3BLabelTicketNumber&Profile=&Name=&Fulltext=&TicketNumber='.$RESULT["ccha_chamado_ticket"].'&Attribute=&ResultForm=Normal';

        echo '<tr style="background-color:'.$cor_linha.';color:'.$cor_linha_fonte.'">
                <td scope="row">'.$RESULT["ccha_chamado_prioridade"].'</td>
                <td>'.$RESULT["cli_nome"].'</td>
                <td>'.$RESULT["ccha_otrs_data_cadastro"].'</td>
                <td>'.$RESULT["ccha_otrs_data_alteracao"].'</td>
                <td>'.substr($RESULT["ccha_otrs_proprietario"],0, strrpos($RESULT["ccha_otrs_proprietario"], '(')).'</td>
                <td>'.$RESULT["ccha_chamado_titulo"].'</td>
                <td>'.$RESULT["ccha_otrs_fila"].'</td>
                <td>'.$RESULT["ccha_otrs_estado"].'</td>
                <td class="'.$cor_status.' '.$cor_fonte.'">'.$RESULT["ccha_chamado_status"].'</td>
                <td><a target="_blank" href="https://dev.azure.com/trafegus/Trafegus%20Transform/_workitems/edit/'.$RESULT["ccha_chamado_tarefa_cod"].'/">'.$RESULT["ccha_chamado_tarefa_cod"].'</a></td>
                <td style="width: 110px;">
                    <div class="td-icon-bandeja">
                        <button class="td-icon-btn"><i class="bi bi-eye-fill td-icon" style="color:'.$cor_acao_icones.'"></i></button>
                        <a onclick="CriaModalEditarChamado('.$RESULT['ccha_codigo'].')" href="" class="td-icon-btn" data-bs-toggle="modal" data-bs-target="#modal-editar-chamado"><i class="bi bi-pencil-square td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                        <a class="td-icon-btn" href="'.$OTRS_Link.'" target="_blank" style="margin-left: 10px;"><i class="bi bi-box-arrow-right td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                    </div>
                </td>
            </tr>';
    }
}
?>