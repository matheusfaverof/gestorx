<?php 
# Recebe os Dados Editados no Modal de Edição de Chamados e Salva as Informações no Banco de Dados

//Recebe os dados do Cadastro de Chamados
$post_prioridade    = $_POST['post-prioridade'];
//$post_datachamado   = $_POST['post-datachamado'];
//$post_ticket        = $_POST['post-ticket'];
//$post_titulo        = $_POST['post-titulo'];
$post_tarefa        = $_POST['post-tarefa'];
$post_fila          = $_POST['post-fila'];
$post_tipo          = $_POST['post-tipo'];
$post_status        = $_POST['post-status'];
$post_observacao    = $_POST['post-observacao'];
$post_pendencias    = $_POST['post-pendencias'];
$post_codigo        = $_POST['post-codigo'];
//$post_alarme        = $_POST['post-SwitchAlarme'];      #Não Implementado
//$post_bloquear      = $_POST['post-SwitchBloquear'];    #Não Implementado

$user_id = $_COOKIE['user_account_id'];                   #Busca o ID do Usuario nos Cookies

//Altera o valor da Variavel
if($post_tarefa == null){
    $post_tarefa = null;
}


include('../../system\db\db_config.php');

#Atualiza o Chamado
$SQL = "UPDATE ccha_cliente_chamado
SET  ccha_chamado_tipo = $1, ccha_chamado_status = $2, ccha_chamado_tarefa_cod = $3, ccha_chamado_prioridade = $4, ccha_chamado_observacao = $5, ccha_chamado_pendencias = $6
WHERE ccha_codigo = $7; ";

$SQL_Atualiza_Chamado = pg_query_params($cconn, $SQL, array($post_tipo, $post_status, $post_tarefa, $post_prioridade, $post_observacao, $post_pendencias, $post_codigo));

$_SESSION['ChamadoEditadoSalvo'] = true;
header('Location: EditarChamadoModal.php?CodigoChamado='.$post_codigo);
?>