<?php
/********************************************************************/
/**                                                                **/
/**                           GestorX                              **/
/**             Todos os Direitos Reselvados - 2023                **/
/**                                                                **/
/**       A Copia parcial ou total deste documento é proibida      **/
/**                                                                **/
/********************************************************************/
# Descrição: Realiza a inserção e carregamento dos Clientes associados ao Usuario no Form Configurações
# @Autor: Matheus Favero

session_start();

// Lista de Clientes retornado pelo POST
$Clientes = null;

// Verifica se o Post é para Salvar a Lista e Chama o Método de Execução
if(isset($_POST['SalvaListaClientes'])) {
    $Clientes = $_POST;                         //Lista de Clientes
    unset($Clientes['SalvaListaClientes']);     //Remove o Nome/Valor do Botão de Submit
    SalvaClientes($Clientes);                   //Inicia a Gravação dos Dados
}

# Função que Busca todos os Clientes cadastrados no Banco de dados
# Flega o SelectBox para os clientes que possuem associação ao usuario Logado

function BuscaClientes(){
    include('./system/db/db_config.php');           //Arquivo de Configuração da Conexão com o DB
    $user_id = $_COOKIE['user_account_id'];


    $pg_result_listaclientes = pg_query($cconn,"SELECT cli_codigo,cli_nome FROM cli_clientes");  //Busca todos os Clientes Cadastrados no Banco

    while ($result = pg_fetch_assoc($pg_result_listaclientes)) {
        
        //Verifica se está Marcado
        $pg_query_verificaMarcado = pg_query($cconn,"SELECT * FROM cg_cliente_gestor cg WHERE cg_usua_codigo = $user_id AND cg.cg_cli_codigo = ".$result['cli_codigo']."");
        $pg_result_verificaMarcado = pg_fetch_assoc($pg_query_verificaMarcado);

        if(isset($pg_result_verificaMarcado['cg_usua_codigo']) == $user_id){
            $Check = 'checked'; //Se Marchado
        } else {
            $Check = null;      //Se Não Marcado
        }
    
        //Impreme os Clientes
        echo '<div class="col-4">
                <div class="form-check cliente-checkbox form-check">
                    <input class="form-check-input" type="checkbox" name="'.$result['cli_nome'].'" value="'.$result['cli_codigo'].'" id="'.$result['cli_nome'].'" '.$Check.'>
                    <label class="form-check-label" for="'.$result['cli_nome'].'">
                        '.$result['cli_nome'].'
                    </label>
                </div>
            </div>';
    }
}

# Função que salva as alterações realizadas na lista de Clientes
# @Param $Clientes: Lista de Clientes

function SalvaClientes($Clientes){
    include('../../system/db/db_config.php');       //Arquivo de Configuração da Conexão com o DB
    $user_id = $_COOKIE['user_account_id'];         //Busca o ID do Usuario nos Cookies


    //Deleta todos os Registros de vinculo do Usuario na ACG
    $pg_query_delete = "DELETE FROM cg_cliente_gestor WHERE cg_usua_codigo = ".$user_id."";
    pg_query($cconn, $pg_query_delete);

    //Grava os Registros Novos
    foreach ($Clientes as $valor) {
        $pg_query_insert = "INSERT INTO public.cg_cliente_gestor(cg_cli_codigo, cg_usua_codigo)
        VALUES(".$valor.",".$user_id.");
        ";
        pg_query($cconn, $pg_query_insert);
    }
    $_SESSION['ListaClientesSalva'] = true;
    header('Location: /Configuracoes.php');
}

?>