<?php 
/********************************************************************/
/**                                                                **/
/**                           GestorX                              **/
/**             Todos os Direitos Reselvados - 2023                **/
/**                                                                **/
/**       A Copia parcial ou total deste documento é proibida      **/
/**                                                                **/
/********************************************************************/
// Descrição: Busca todos os dados de Um Chamado especifico
// @Autor: Matheus Favero



# Busca todos os dados de um Chamado especifico, utilizado na Edição de Chamados
# @Param $Codigo: Codigo do Chamado
# @Return: Retorna os Dados encontrados

function BuscaDadosChamadosEdit($Codigo){
    include('./../../system\db\db_config.php');
    $user_id = pg_escape_string($cconn, $_COOKIE['user_account_id']); #Busca o ID do Usuario nos Cookies

    $SQL_Busca_Chamado = ("SELECT * FROM ccha_cliente_chamado ccha
    JOIN cli_clientes cc ON cc.cli_codigo = ccha.ccha_cli_codigo 
    WHERE ccha.ccha_codigo = $Codigo");

    $RESUL_Busca_Chamado = pg_query($cconn, $SQL_Busca_Chamado);
    $RESULT = pg_fetch_assoc($RESUL_Busca_Chamado);
    return $RESULT;
}
?>