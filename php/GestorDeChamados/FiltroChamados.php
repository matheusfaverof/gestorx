<?php 
/********************************************************************/
/**                                                                **/
/**                           GestorX                              **/
/**             Todos os Direitos Reselvados - 2023                **/
/**                                                                **/
/**       A Copia parcial ou total deste documento é proibida      **/
/**                                                                **/
/********************************************************************/
// Descrição:   Controle do Carregamento das informações do Monitor Gestor de Chamados
//              Controla os Filtros do Monitor
// @Autor:      Matheus Favero



session_start();

### Regras ###
# Recebe o POST da pagina de chamados e valida os dados dos Filtros
# Após realizada um GET no WS e solicita atualizar os chamados do cliente necessário
# Seta as sessões com os valores
# Após retorna para a pagina de chamados onde será realizo o carregamento das informações salvas no banco


$Filtro_Texto = null;       //Texto inserido no Filtro

// Verifica se foi enviado um Filtro pelo POST
if(isset($_POST['filtro-busca'])){
    $Filtro_Texto = $_POST['filtro-busca'];                                  //Valor do Filtro de Texto
}

//Verifica se o Filtro Texto está Vazio (Carregamento Padrão do Monitor)
if($Filtro_Texto == '' || $Filtro_Texto == null){
    #Caso o Filtro de Texto não contenha dados para realizar a busca (Primeiro carregamento da Pagina)
    $RetornoWS = WS_AtualizaChamadosSobGestao();

    if($RetornoWS == 'Chamados Atualizados'){
        sleep(2);
        header('Location: /GestorDeChamados.php'); 
    } else {
        echo 'Ocorreu um erro ao obter os chamados sob gestão via WS! Cod: 1';
        echo $RetornoWS;
    }
} else if($Filtro_Texto != '' && $Filtro_Texto != null){
    #Caso o Filtro de Texto contenha Dados
    $RetornoWS = WS_AtualizaChamadosSobGestao();

    if($RetornoWS == 'Chamados Atualizados'){
        $_SESSION['Filtro-Chamados'] = $Filtro_Texto;
        sleep(2);
        header('Location: /GestorDeChamados.php'); 
    } else {
        echo 'Ocorreu um erro ao obter os chamados sob gestão via WS! Cod: 2';
        echo $RetornoWS;
    }
} else {
    echo 'Ocorreu um erro ao validar os dados do Filtro pelo WS, contate o Administrador!';
}

# Função que realiza a Chamada do Método do WebService
# @Return: Retorna a resposta do Servidor

function WS_AtualizaChamadosSobGestao(){
    $user_id = $_COOKIE['user_account_id']; //Busca o ID do Usuario nos Cookies

    $Host = curl_init('http://100.96.1.2:8080/BuscaChamadosSobGestao?Codigo='.$user_id);

    curl_setopt_array($Host, [
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_RETURNTRANSFER => 1,
    ]);
    $Resposta = json_decode(curl_exec($Host), true);
    curl_close($Host);
    echo '<br>Resposta do Servidor: '.$Resposta;
    echo '<br>ID do Usuario: '. $user_id;
    return $Resposta;
}
?>