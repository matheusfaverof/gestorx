<?php 





function ListaChamadosSemFiltro() {
    # Busca todos os Chamados sob gestão do Usuario Logado
    # Não considera filtros, sendo o primeiro carregamento da pagina

    include('./system/db/db_config.php');
    $user_id = $_COOKIE['user_account_id'];         #Busca o ID do Usuario nos Cookies

    $PG_QUERY_CHAMADOS = pg_query($cconn, "SELECT * FROM ccha_cliente_chamado ccha
	JOIN cli_clientes cc ON cc.cli_codigo = ccha.ccha_cli_codigo
	WHERE ccha_otrs_fila in('Análise', 'Atendimento', 'Infraestrutura', 'Projetos')
    AND (ccha_chamado_status != 'Concluido' OR ccha_chamado_status IS NULL)
	AND ccha.ccha_otrs_proprietario ilike '%' || (SELECT usua_nome FROM usua_usuario uu where usua_codigo = $user_id) || '%'
	ORDER BY ccha.ccha_data_cadastro DESC");

    while($RESULT = pg_fetch_assoc($PG_QUERY_CHAMADOS)) {
        //Variaveis do Resultado
        $cor_status = null;
        $cor_fonte  = null;
        $cor_linha  = null;
        $cor_linha_fonte = null;
        $cor_acao_icones = null;

        //Validações
        if($RESULT['ccha_chamado_status'] != 'Concluido' && $RESULT['ccha_otrs_estado'] == 'Encerrado'){
            $cor_linha = 'Black';
            $cor_linha_fonte = 'White';
            $cor_acao_icones = 'White';
        }

        //Seletor de Cor do Status
        switch($RESULT["ccha_chamado_status"]){
            case "Novo":
                $cor_status = "bg-novo";
                $cor_fonte = "fonte-white";
                break;
            case "Em Analise":
                $cor_status = "bg-emanalise";
                $cor_fonte = "fonte-white";
                break;
            case "Pendente":
                $cor_status = "bg-danger";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando ATT":
                $cor_status = "bg-aguardandoatt";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando Cliente":
                $cor_status = "bg-aguardandocliente";
                $cor_fonte = "fonte-white";
                break;
            case "Concluido":
                $cor_status = "bg-concluido";
                $cor_fonte = "fonte-black";
            break;
        }

        //Constroi o Link do Chamado para direcionar ao OTRS
        $OTRS_Link = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?ChallengeToken=THGYujeIDdKWFvEa88aH1VmWDlLQ62nz&Action=AgentTicketSearch&Subaction=Search&EmptySearch=1&ShownAttributes=LabelFulltext%3BLabelTicketNumber&Profile=&Name=&Fulltext=&TicketNumber='.$RESULT["ccha_chamado_ticket"].'&Attribute=&ResultForm=Normal';

        echo '<tr style="background-color:'.$cor_linha.';color:'.$cor_linha_fonte.'">
                <td scope="row">'.$RESULT["ccha_chamado_prioridade"].'</td>
                <td>'.$RESULT["cli_nome"].'</td>
                <td>'.$RESULT["ccha_otrs_data_cadastro"].'</td>
                <td>'.$RESULT["ccha_otrs_data_alteracao"].'</td>
                <td>'.$RESULT["ccha_chamado_ticket"].'</td>
                <td>'.$RESULT["ccha_chamado_titulo"].'</td>
                <td>'.$RESULT["ccha_otrs_fila"].'</td>
                <td>'.$RESULT["ccha_otrs_estado"].'</td>
                <td class="'.$cor_status.' '.$cor_fonte.'">'.$RESULT["ccha_chamado_status"].'</td>
                <td><a target="_blank" href="https://dev.azure.com/trafegus/Trafegus%20Transform/_workitems/edit/'.$RESULT["ccha_chamado_tarefa_cod"].'/">'.$RESULT["ccha_chamado_tarefa_cod"].'</a></td>
                <td style="width: 110px;">
                <div class="td-icon-bandeja">
                    <button class="td-icon-btn"><i class="bi bi-eye-fill td-icon" style="color:'.$cor_acao_icones.'"></i></button>
                    <a onclick="CriaModalEditarChamado('.$RESULT['ccha_codigo'].')" href="" class="td-icon-btn" data-bs-toggle="modal" data-bs-target="#modal-editar-chamado"><i class="bi bi-pencil-square td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                    <a class="td-icon-btn" href="'.$OTRS_Link.'" target="_blank" style="margin-left: 10px;"><i class="bi bi-box-arrow-right td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                </div>
                </td>
             </tr>';
    }
}



function ListaChamadosComFiltro($Filtro_Texto){
# Busca todos os Chamados
    # Considera o Filtro de Texto

    include('./system/db/db_config.php');
    $user_id = $_COOKIE['user_account_id'];         #Busca o ID do Usuario nos Cookies

    $PG_QUERY_CHAMADOS = "SELECT * FROM ccha_cliente_chamado ccha
    JOIN cli_clientes cc on cc.cli_codigo = ccha_cli_codigo 
    WHERE ccha_otrs_fila in('Análise', 'Atendimento', 'Infraestrutura', 'Projetos')
    AND (ccha_chamado_status != 'Concluido' OR ccha_chamado_status IS NULL)
    AND ccha.ccha_otrs_proprietario ilike '%' || (SELECT usua_nome FROM usua_usuario uu where usua_codigo = $1) || '%'
	AND (ccha_chamado_prioridade ilike $2
            OR ccha_otrs_data_cadastro = $3
            OR ccha_otrs_data_alteracao = $3
            OR ccha_chamado_ticket ilike $2
            OR ccha_chamado_titulo ilike $2
            OR ccha_otrs_fila ilike $2
            OR ccha_chamado_status ilike $2
            OR ccha_chamado_tarefa_cod ilike $2)
    ORDER BY ccha.ccha_data_cadastro DESC";
    
    $PG_RESULT_CHAMADOS = pg_query_params($cconn, $PG_QUERY_CHAMADOS, array($user_id, '%'.$Filtro_Texto.'%',null));

    while($RESULT = pg_fetch_assoc($PG_RESULT_CHAMADOS)){
        //Variaveis do Resultado
        $cor_status = null;
        $cor_fonte  = null;
        $cor_linha  = null;
        $cor_linha_fonte = null;
        $cor_acao_icones = null;

        //Validações
        if($RESULT['ccha_chamado_status'] != 'Concluido' && $RESULT['ccha_otrs_estado'] == 'Encerrado'){
            $cor_linha = 'black';
            $cor_linha_fonte = 'white';
            $cor_acao_icones = 'white';
        }

        //Seletor de Cor do Status
        switch($RESULT["ccha_chamado_status"]){
            case "Novo":
                $cor_status = "bg-novo";
                $cor_fonte = "fonte-white";
                break;
            case "Em Analise":
                $cor_status = "bg-emanalise";
                $cor_fonte = "fonte-white";
                break;
            case "Pendente":
                $cor_status = "bg-danger";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando ATT":
                $cor_status = "bg-aguardandoatt";
                $cor_fonte = "fonte-white";
                break;
            case "Aguardando Cliente":
                $cor_status = "bg-aguardandocliente";
                $cor_fonte = "fonte-white";
                break;
            case "Concluido":
                $cor_status = "bg-concluido";
                $cor_fonte = "fonte-black";
            break;
        }

        //Constroi o Link do Chamado para direcionar ao OTRS
        $OTRS_Link = 'https://suporte.chapeco-solucoes.com.br/otrs/index.pl?ChallengeToken=THGYujeIDdKWFvEa88aH1VmWDlLQ62nz&Action=AgentTicketSearch&Subaction=Search&EmptySearch=1&ShownAttributes=LabelFulltext%3BLabelTicketNumber&Profile=&Name=&Fulltext=&TicketNumber='.$RESULT["ccha_chamado_ticket"].'&Attribute=&ResultForm=Normal';

        echo '<tr style="background-color:'.$cor_linha.';color:'.$cor_linha_fonte.'">
                <td scope="row">'.$RESULT["ccha_chamado_prioridade"].'</td>
                <td>'.$RESULT["cli_nome"].'</td>
                <td>'.$RESULT["ccha_otrs_data_cadastro"].'</td>
                <td>'.$RESULT["ccha_otrs_data_alteracao"].'</td>
                <td>'.$RESULT["ccha_chamado_ticket"].'</td>
                <td>'.$RESULT["ccha_chamado_titulo"].'</td>
                <td>'.$RESULT["ccha_otrs_fila"].'</td>
                <td>'.$RESULT["ccha_otrs_estado"].'</td>
                <td class="'.$cor_status.' '.$cor_fonte.'">'.$RESULT["ccha_chamado_status"].'</td>
                <td><a target="_blank" href="https://dev.azure.com/trafegus/Trafegus%20Transform/_workitems/edit/'.$RESULT["ccha_chamado_tarefa_cod"].'/">'.$RESULT["ccha_chamado_tarefa_cod"].'</a></td>
                <td style="width: 110px;">
                    <div class="td-icon-bandeja">
                        <button class="td-icon-btn"><i class="bi bi-eye-fill td-icon" style="color:'.$cor_acao_icones.'"></i></button>
                        <a onclick="CriaModalEditarChamado('.$RESULT['ccha_codigo'].')" href="" class="td-icon-btn" data-bs-toggle="modal" data-bs-target="#modal-editar-chamado"><i class="bi bi-pencil-square td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                        <a class="td-icon-btn" href="'.$OTRS_Link.'" target="_blank" style="margin-left: 10px;"><i class="bi bi-box-arrow-right td-icon" style="color:'.$cor_acao_icones.'"></i></a>
                    </div>
                </td>
            </tr>';
    }
}

?>