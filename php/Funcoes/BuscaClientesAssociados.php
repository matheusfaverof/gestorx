<?php

function BuscaClientesAssociados(){
    ## Busca todos os Clientes que possuem associação com o Usuario

    include('./system/db/db_config.php');
    $user_id = $_COOKIE['user_account_id'];         #Busca o ID do Usuario nos Cookies

    $PG_QUERY_BUSCA_CLIENTES = pg_query($cconn,"SELECT cc.cli_codigo,cc.cli_nome FROM cg_cliente_gestor cg JOIN cli_clientes cc ON cc.cli_codigo = cg.cg_cli_codigo WHERE cg.cg_usua_codigo = $user_id");

    $RESULT = pg_fetch_assoc($PG_QUERY_BUSCA_CLIENTES);
    return $RESULT['cli_codigo'];
}

?>