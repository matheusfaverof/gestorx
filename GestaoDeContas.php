<?php 
session_start();
include("./php/Login/Login-Validador.php");
include("./php/GestaoDeContas/BuscaListaClientesGestor.php");
include("./php/GestaoDeContas/ListaChamados.php");
?>


<html>
    <head>
        <!-- OI, oque está fazendo aqui ? -->
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.3/font/bootstrap-icons.css">
        <link href="css/GestaoDeContas.css" rel="stylesheet">
        <link href="css/navbar.css" rel="stylesheet">   <!-- NavBar -->
        <script src="./js/loadingpage.js"></script>     <!-- Loading -->
        <link href="css/loading.css" rel="stylesheet">  <!-- Loading -->
        <title>GestorX - Chamados por Cliente</title>
    </head>

    <body onload="IniciaPopovers()">

        <script type="text/javascript">

            

            function IniciaPopovers(){
                //Inicia os Popovers
                const popoverTriggerList = document.querySelectorAll('[data-bs-toggle="popover"]')
                const popoverList = [...popoverTriggerList].map(popoverTriggerEl => new bootstrap.Popover(popoverTriggerEl))
            }

            //Cria um Modal para editar os Chamados
            function CriaModalEditarChamado($CodigoChamado){

                //Deleta o conteudo do Modal
                var modal_body = document.getElementById('modal-editchamado-corpo');
                modal_body.innerHTML = '';

                //Cria um Iframe e envia o codigo do Chamado
                $(document).ready(function(){
                    var modal_body = $('#modal-editchamado-corpo');
                    modal_body.append('<iframe onload="ControleModal()" id="edit-iframe" src="/php/GestaoDeContas/EditarChamadoModal.php?CodigoChamado=' + $CodigoChamado + '" style="width: 100%;height: 700px"></iframe>');
                });
            }


            //Controla o Modal Editar chamado dentro do Iframe
            function ControleModal(){
                // Função chamada pelo Evento OnLoad colocado no Iframe
                // Busca o document do Iframe
                // Verifica se o Botão contino no Iframe foi clicado
                // Se clicado fecha o Modal

                setTimeout(1000);
                var iframe = document.getElementById("edit-iframe").contentWindow.document; //Busca o Documento do Iframe
                var btn = iframe.getElementById("salvar-btn");                              //Busca o Botão Dentro do Iframe
                btn.addEventListener("click", function (e) {                                //Verifica se o Botão foi Clicado
                    jQuery('#modal-editar-chamado').modal('hide');                          //Fecha o Modal na pagina atual
                    IniciarLoading();
                    setTimeout(function(){ 
                        window.location.reload(true);
                    }, 2000);
                });
            }

        </script>
            
        <!-- Pagina de Carregamento -->
        <div id="loading" style="display: none;" class="spinner-wrapper">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>

        <!-- NavBar -->
        <header id="header">
            <nav class="navbar navbar-expand-lg navbar-dark" aria-label="Fifth navbar example">
                <div class="container-fluid">
                    <a class="navbar-brand" id="header-title" href="#">GestorX</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- Menu Equerda-->
                    <div class="collapse navbar-collapse" id="navbarsExample05">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/Home.php">Home</a>
                            </li>
                            <li class="nav-item dropdown">
                                <button class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style="backGround-color: transparent;border:none;">
                                    Monitores
                                </button>
                                <ul class="dropdown-menu" style="color:white;font-size:12px;">
                                    <li><a class="dropdown-item" href="./GestaoDeContas.php">Gestão de Contas</a></li>
                                    <li><a class="dropdown-item" href="./GestorDeChamados.php">Gestor de Chamados</a></li>
                                </ul>
                            </li>
                        </ul>

                        <!-- Menu Direita-->
                        <div>
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="./Configuracoes.php">Configurações</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <!-- Container-->
        <div class="container2" id="container">

            <!-- Editar Chamado -->
            <!-- Modal -->           
            <div class="modal fade" id="modal-editar-chamado" tabindex="-1" data-bs-backdrop="static" aria-labelledby="modal-edit-chamado" aria-hidden="true">
                <div class="modal-dialog modal-xl" style="width: 1100px;">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="btn-close shadow-none" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body" id="modal-editchamado-corpo">
                        </div>
                    </div>
                </div>
            </div>

            <!-- Filtro -->
            <form id="filtro-box" method="post" action="./php/GestaoDeContas/FiltroChamados.php" target="_self">

                <!-- Filtro por Cliente -->
                <div id="filtro-box-esquerda">
                    <div class="input-group">
                        <select class="form-select form-select-sm" aria-label=".form-select-sm example" name="filtro-cliente">
                            <option value="0">Todos</option>
                            <?php BuscaListaClientesGestor(); ?>
                            <!--<option value="3">Todos</option>-->
                        </select>
                        <button class="btn btn-primary btn-sm" type="submit" id="button-filtro-buscar" onclick="IniciarLoading()">Atualizar</button>
                    </div>
                </div>                
                <!-- Buscar Input -->
                <div id="filtro-box-direita">
                    <!-- <div id="info-btn"><i class="bi bi-info-square"></i></div> -->
                    <button type="button" id="info-btn" data-bs-custom-class="info-popover" data-bs-toggle="popover" data-bs-title="Ajuda"
                        data-bs-content="OS Chamados exibidos são apenas dos Clientes selecionados na aba Configurações, e que estejam com status Aberto/Em Atendimento no OTRS.
                        Caso um chamado for fechado no OTRS porem estiver com Status Aberto no GestorX, a linha será pintada na cor preta!"><i class="bi bi-info-square"></i></button>
                    
                    
                    
                    
                    
                    <div class="input-group" id="btn-buscar">
                        <input type="text" class="form-control form-control-sm" placeholder="Buscar..." value="" id="input-busca" name="filtro-busca" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn btn-primary btn-sm" type="submit" id="button-filtro-buscar" onclick="IniciarLoading()">Buscar</button>
                       <!-- <a class="btn btn-warning btn-sm" type="button" id="button-filtro-limpar" href="./chamados.php">Limpar</a> -->
                    </div>   
                </div>
            </form>



            <!-- Tabela -->
            <table class="table" id="tabela-box">
                <thread>
                    <tr>
                        <!-- https://cursos.alura.com.br/forum/topico-como-eu-poderia-fazer-para-ordenar-a-tabela-67980 -->
                        <th scope="col" class="cabeçalho-item">Prioridade</th>
                        <th scope="col" class="cabeçalho-item">Cliente</th>
                        <th scope="col" class="cabeçalho-item">Data Cadastro</th>
                        <th scope="col" class="cabeçalho-item">Data Alteração OTRS</th>
                        <th scope="col" class="cabeçalho-item">Proprietário</th>
                        <th scope="col" class="cabeçalho-item">Titulo</th>
                        <th scope="col" class="cabeçalho-item">Fila OTRS</th>
                        <th scope="col" class="cabeçalho-item">Estado OTRS</th>
                        <th scope="col" class="cabeçalho-item">Status</th>
                        <th scope="col" class="cabeçalho-item">Tarefa</th>
                        <th scope="col" class="cabeçalho-item" style="text-align: center;">Ações</th>
                    </tr>
                </thread>
                <tbody style="font-size: 12px;" id="tabela-chamados-corpo">

                    <!-- Lista todos os Chamados | Sem Filto -->
                    <?php 
                        if(!isset($_SESSION['Filtro-Cliente'])){
                            ListaChamadosClienteSemFiltro(); 
                        }    
                    ?>

                    <!-- Lista os chamados Respeitando os Filtros Selecionados -->
                    <?php 
                        if(isset($_SESSION['Filtro-Cliente'])){
                            $Cliente    = $_SESSION['Filtro-Cliente'][0];
                            $Texto      = $_SESSION['Filtro-Cliente'][1];
                            ListaChamadosClienteComFiltro($Cliente,$Texto);
                        }
                    ?>
                </tbody>
            </table>
        </div>

        <!-- jQuery -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>