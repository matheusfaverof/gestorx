<?php 
    include("./php/Login/Login-Validador.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
        <link href="css/home.css" rel="stylesheet">
        <link href="css/navbar.css" rel="stylesheet">
        <script src="./js/loadingpage.js"></script>     <!-- Loading -->
        <link href="css/loading.css" rel="stylesheet">  <!-- Loading -->
        <title>GestorX - Main Page</title>
    </head>
    <body>

        <!-- Pagina de Carregamento -->
        <div id="loading" style="display: none;" class="spinner-wrapper">
            <div class="sk-cube-grid">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
            </div>
        </div>

        <!-- NavBar -->
        <header id="header">
            <nav class="navbar navbar-expand-lg navbar-dark" aria-label="Fifth navbar example">
                <div class="container-fluid">
                    <a class="navbar-brand" id="header-title" href="#">GestorX</a>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample05" aria-controls="navbarsExample05" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <!-- Menu Equerda-->
                    <div class="collapse navbar-collapse" id="navbarsExample05">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/Home.php">Home</a>
                            </li>
                            <li class="nav-item dropdown">
                                <button class="nav-link dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" style="backGround-color: transparent;border:none;">
                                    Monitores
                                </button>
                                <ul class="dropdown-menu" style="color:white;font-size:12px;">
                                    <li><a class="dropdown-item" href="./php/GestaoDeContas/FiltroChamados.php" onclick="IniciarLoading()">Gestão de Contas</a></li>
                                    <li><a class="dropdown-item" href="./php/GestorDeChamados/FiltroChamados.php" onclick="IniciarLoading()">Gestor de Chamados</a></li>
                                </ul>
                            </li>
                        </ul>

                        <!-- Menu Direita-->
                        <div>
                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                <li class="nav-item">
                                    <a class="nav-link" href="./Configuracoes.php">Configurações</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <div id="container">
            
        </div>
        
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script>
    </body>
</html>