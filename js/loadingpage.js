// Função que Inicia a Tela de Carregamento

function IniciarLoading(){
    const $menu = document.getElementById("header");
    const $conteudo = document.getElementById("container");
    const $loading = document.getElementById("loading");

    $conteudo.setAttribute('style', 'display: none;')
    $menu.setAttribute('style', 'display: none;');
    $loading.removeAttribute('style', 'display');
}